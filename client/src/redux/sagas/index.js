import { takeLatest, call, put } from "redux-saga/effects";
import * as actions from '../actions';
import * as api from '../../api';

function* fetchPostsSaga(action) {
  try {
    const posts = yield call(api.fetchPosts);
    console.log('[posts]', posts);
    yield put(actions.getPosts.getPostsSuccess(posts.data));
  } catch (err) { console.error(err); yield put(actions.getPosts.getPostsFailure(err)); }

}

function* createPostSaga(action) {
  try {
    const post = yield call(api.createPost, action.payload);
    yield put(actions.createPost.createPostSuccess(post.data));
  } catch (err) {
    console.error(err);
    yield put(actions.createPost.createPostFailure(err));
  }
}

function* updatePostSaga(action) {
  try {
    console.log('updatePostSagas', { action });
    const updatedPost = yield call(api.updatePost, action.payload);
    console.log('[updatedPostSagas - post]', updatedPost);
    yield put(actions.updatePost.updatePostSuccess(updatedPost.data));
  } catch (err) {
    console.error(err);
    yield put(actions.updatePost.updatePostFailure(err));
  }
}

function* deletePostSaga(action) {
  try {
    if (!action.payload || !action.payload._id) {
      throw new Error("Invalid payload: _id is missing");
    }
    console.log('deletePostSagas', { action });
    const deletePost = yield call(api.deletePost, action.payload._id);
    console.log('[deletePostSagas - post]', deletePost);
    yield put(actions.deletePost.deletePostSuccess(deletePost.data));
  } catch (err) {
    console.error(err);
    yield put(actions.deletePost.deletePostFailure(err));
  }
}

function* mySaga() {
  yield takeLatest(actions.getPosts.getPostsRequest, fetchPostsSaga);
  yield takeLatest(actions.createPost.createPostRequest, createPostSaga);
  yield takeLatest(actions.updatePost.updatePostRequest, updatePostSaga);
  yield takeLatest(actions.deletePost.deletePostRequest, deletePostSaga);


}

export default mySaga;