import { Button, Modal, TextareaAutosize, TextField } from '@material-ui/core';
import FileBase64 from 'react-file-base64';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { modalState$ } from '../../redux/selectors';
import useStyles from './styles';
import { createPost, hideModal } from '../../redux/actions';

export default function CreatePostModal() {

  const [data, setData] = React.useState({
    title: '',
    content: '',
    attachment: '',
  });
  const dispatch = useDispatch();
  const { isShow } = useSelector(modalState$);
  console.log(isShow);
  const classes = useStyles();

  const onClose = React.useCallback(() => {
    dispatch(hideModal());
    setData({
      title: '',
      content: '',
      attachment: '',
    })
  }, [dispatch]);

  const onSubmit = React.useCallback(() => {
    console.log({ data });
    dispatch(createPost.createPostRequest(data));
    onClose();
  }, [data, dispatch, onClose]);

  const body = (
    <div className={classes.paper}>
      <h2> Create New Post</h2>
      <form noValidate autoComplete='off' className={classes.form}>
        <TextField
          className={classes.title}
          required Label='Title'

          value={data.this}
          onChange={(e) => {
            setData({ ...data, title: e.target.value })
          }}
        >

        </TextField>
        <TextareaAutosize
          className={classes.textarea}
          rowsMin={10}
          rowsMax={15}
          placeholder='Content...'
          value={data.content}
          onChange={(e) => {
            setData({ ...data, content: e.target.value })
          }}
        >
        </TextareaAutosize>
        <FileBase64
          accept="image/*"
          multiple={false}
          type='file'
          value={data.attachment}
          onDone={({ base64 }) => {
            setData({ ...data, attachment: base64 })
          }}
        ></FileBase64>
        <div className={classes.footer}>
          <Button
            variant='contained' color='primary' component='span' fullWidth={true} onClick={onSubmit}
          >
            Create
          </Button>
        </div>

      </form>

    </div>
  );
  return (
    <div>
      <Modal open={isShow} onClose={onClose}>
        {body}
      </Modal>

    </div>
  );
}
