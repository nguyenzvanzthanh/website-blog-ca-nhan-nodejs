import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import mongoose from "mongoose";
import posts from "./routers/posts.js";
import dbConfig from "./config/db.config.js";
import db from "./models/index.js";
import authRoutes from "./routers/auth.routes.js";
import userRoutes from "./routers/user.routes.js";
const app = express();

const PORT = process.env.PORT || 5000;

app.use(bodyParser.json({limit:'30mb'}));
app.use(bodyParser.urlencoded({extended:true,limit:'30mb'}));
app.use(cors());

app.use('/posts',posts);
authRoutes(app);
userRoutes(app);

const Role = db.role;

db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
    app.listen(PORT, () =>{
        console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });


  async function initial() {
    try {
      const count = await Role.estimatedDocumentCount();
      if (count === 0) {
        // Tạo và lưu vai trò mới
        await new Role({ name: "user" }).save();
        console.log("added 'user' to roles collection");
        await new Role({ name: "moderator" }).save();
        console.log("added 'moderator' to roles collection");

        await new Role({ name: "admin" }).save();
        console.log("added 'admin' to roles collection");
      }
    } catch (err) {
      console.log("error", err);
    }
  }
  
