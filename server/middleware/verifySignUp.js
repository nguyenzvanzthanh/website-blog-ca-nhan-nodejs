import db from "../models/index.js";
const ROLES = db.ROLES;
const User = db.user;

const handleDatabaseError = (res, err) => {
  res.status(500).send({ message: err });
};

const checkDuplicateUsernameOrEmail = async (req, res, next) => {
  try {
    const existingUsername = await User.findOne({ username: req.body.username });
    if (existingUsername) {
      return res.status(400).send({ message: "Failed! Username is already in use!" });
    }

    const existingEmail = await User.findOne({ email: req.body.email });
    if (existingEmail) {
      return res.status(400).send({ message: "Failed! Email is already in use!" });
    }

    next();
  } catch (err) {
    handleDatabaseError(res, err);
  }
};

const checkRolesExisted = (req, res, next) => {
  if (!req.body.roles || !Array.isArray(req.body.roles)) {
    return res.status(400).send({ message: "Failed! No roles provided or roles is not an array!" });
  }

  for (let i = 0; i < req.body.roles.length; i++) {
    if (!ROLES.includes(req.body.roles[i])) {
      return res.status(400).send({
        message: `Failed! Role ${req.body.roles[i]} does not exist!`
      });
    }
  }

  next();
};

const verifySignUp = {
  checkDuplicateUsernameOrEmail,
  checkRolesExisted
};

export default verifySignUp;
