import authJwt from "./authJwt";
import verifySignUp from "./verifySignUp";

const exports = {
    authJwt,
    verifySignUp
  };
  
  export default exports;