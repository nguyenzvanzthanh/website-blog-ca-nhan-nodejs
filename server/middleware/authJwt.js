import jwt from "jsonwebtoken";
import config from "../config/auth.config.js";
import db from "../models/index.js";

const User = db.user;
const Role = db.role;

const verifyToken = async (req, res, next) => {
  try {
    const token = req.headers["x-access-token"];
    if (!token) {
      return res.status(403).send({ message: "No token provided!" });
    }

    const decoded = await jwt.verify(token, config.secret);
    req.userId = decoded.id;
    next();
  } catch (err) {
    return res.status(401).send({ message: "Unauthorized!" });
  }
};

const checkUserRole = async (req, res, next, roleName) => {
  try {
    const user = await User.findById(req.userId).exec();
    if (!user) {
      return res.status(500).send({ message: "User not found!" });
    }

    const roles = await Role.find({ _id: { $in: user.roles } }).exec();
    const roleNames = roles.map(role => role.name);
    if (!roleNames.includes(roleName)) {
      return res.status(403).send({ message: `Require ${roleName} Role!` });
    }

    next();
  } catch (err) {
    return res.status(500).send({ message: err });
  }
};

const isAdmin = (req, res, next) => {
  checkUserRole(req, res, next, "admin");
};

const isModerator = (req, res, next) => {
  checkUserRole(req, res, next, "moderator");
};

const authJwt = {
  verifyToken,
  isAdmin,
  isModerator
};

export default authJwt;
