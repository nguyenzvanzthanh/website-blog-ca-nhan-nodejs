import db from "../models/index.js";
const User = db.user;

const allAccess = (req, res) => {
    res.status(200).send("Public Content.");
  };
  
const userBoard = (req, res) => {
    res.status(200).send("User Content.");
  };
  
const adminBoard = (req, res) => {
    res.status(200).send("Admin Content.");
  };
  
const moderatorBoard = (req, res) => {
    res.status(200).send("Moderator Content.");
  };


 const updateUser = async (req, res) => {
  const userId = req.params.userId;
  const updatedFields = req.body;

  try {
    const user = await User.findByIdAndUpdate(userId, updatedFields, { new: true });

    if (!user) {
      return res.status(404).send({ message: "User not found" });
    }

    res.status(200).send({ message: "User updated successfully", user });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};
const deleteUser = async (req, res) => {
    try {
        const id = req.params.userId;
        const user = await User.findOneAndDelete({ _id: id });

        if (!user) {
            return res.status(404).send({ message: "User not found" });
        }

        res.status(200).json(user);
    } catch (err) {    
        res.status(500).json({ error: err });
    }
};

  export default {allAccess,userBoard,adminBoard,deleteUser,moderatorBoard,updateUser};